# -*- coding: utf-8 -*-

from magnetic_field import MagneticField, \
                           MagneticFieldFactory

from constant_magnetic_field import ConstantMagneticField, \
                                    ConstantMagneticFieldFactory

from wmap3yr_magnetic_field import WMAP3yrMagneticField, \
                                   WMAP3yrMagneticFieldFactory
